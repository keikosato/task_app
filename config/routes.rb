Rails.application.routes.draw do

  
  devise_for :users, :controllers => {
    :registrations => "users/registrations",
  }
  resources :cases
  resources :admin
  resources :infos
  root 'cases#index'
 
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
