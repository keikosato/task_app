class CreateCases < ActiveRecord::Migration[5.1]
  def change
    create_table :cases do |t|


    t.string :title
    t.text :memo
    t.integer :status
    t.integer :category
    t.integer :user_id

      t.timestamps
    end
  end
end
